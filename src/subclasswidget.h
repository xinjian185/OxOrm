
#ifndef NEURALNETWORKCONFIGURATION_SUBCLASSWIDGET_H
#define NEURALNETWORKCONFIGURATION_SUBCLASSWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QTableWidget>

class NetworkSubclass;

class SubclassWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SubclassWidget(QWidget *parent = nullptr);
    ~SubclassWidget() override;

    void getSubclass();

signals:
    void sig_subclassChange(const QStringList &list);

public slots:
    void categoryChanged(const QStringList &list);

protected:
    void initUi();
    void getNetworkSubclass();

protected slots:
    void addSubclass();
    void disableSubclass();
    void picSubclass();
    void enableSubclass();

private:
    QComboBox *category_box_{nullptr};
    QComboBox *subclass_useful_box_{nullptr};
    QComboBox *can_repeat_box_{nullptr};

    QLineEdit *subclass_line_{nullptr};
    QLineEdit *subclass_pic_line_{nullptr};

    QLineEdit *top_point_line_{nullptr};
    QLineEdit *bottom_point_line_{nullptr};
    QLineEdit *left_point_line_{nullptr};
    QLineEdit *right_point_line_{nullptr};
    QLineEdit *class_name_line_{nullptr};

    QTableWidget *table_subclass_{nullptr};


    QList<NetworkSubclass> subclasses_;
};


#endif //NEURALNETWORKCONFIGURATION_SUBCLASSWIDGET_H
