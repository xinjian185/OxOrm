
#ifndef NEURALNETWORKCONFIGURATION_SQLBASE_H
#define NEURALNETWORKCONFIGURATION_SQLBASE_H

#include "precompiled.h"

void initDataBase()
{
    qx::QxSqlDatabase::getSingleton()->setDriverName("QSQLITE");
    qx::QxSqlDatabase::getSingleton()->setDatabaseName("EDI.db");
    qx::QxSqlDatabase::getSingleton()->setHostName("localhost");
    qx::QxSqlDatabase::getSingleton()->setUserName("root");
    qx::QxSqlDatabase::getSingleton()->setPassword("");
}

#endif //NEURALNETWORKCONFIGURATION_SQLBASE_H
