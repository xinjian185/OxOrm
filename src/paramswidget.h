
#ifndef NEURALNETWORKCONFIGURATION_PARAMSWIDGET_H
#define NEURALNETWORKCONFIGURATION_PARAMSWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QTableWidget>

class ParamsWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ParamsWidget(QWidget *parent = nullptr);
    ~ParamsWidget() override;

public slots:
    void subclassChanged(const QStringList &list);

protected:
    void initUi();
    void getAllParamsBySubclass(const QString &text);

protected slots:
    void addParam();
    void deleteParam();

private:
    QComboBox *subclass_box_{nullptr};
    QTableWidget *table_params_{nullptr};
    QLineEdit *param_name_line_;
    QLineEdit *param_values_line_;
    QComboBox *param_type_box_;
};


#endif //NEURALNETWORKCONFIGURATION_PARAMSWIDGET_H
