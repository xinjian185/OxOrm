
#ifndef NEURALNETWORKCONFIGURATION_MAINWINDOW_H
#define NEURALNETWORKCONFIGURATION_MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QTabWidget>
#include <QComboBox>
#include "networkcategory.h"
#include "categorywidget.h"
#include "subclasswidget.h"
#include "paramswidget.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

protected:
    void initUi();

protected slots:

private:
    QTabWidget *tab_widget_{nullptr};
    CategoryWidget *category_widget_{nullptr};
    SubclassWidget *subclass_widget_{nullptr};
    ParamsWidget *params_widget_{nullptr};
    QTableWidget *table_subclass_{nullptr};

    QLineEdit *category_line_{nullptr};
    QLineEdit *subclass_line_{nullptr};
    QLineEdit *subclass_pic_line_{nullptr};

    QComboBox *category_box_{nullptr};
    QComboBox *subclass_useful_box_{nullptr};
};


#endif //NEURALNETWORKCONFIGURATION_MAINWINDOW_H
