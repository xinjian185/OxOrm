
#ifndef NEURALNETWORKCONFIGURATION_NETWORKCATEGORY_H
#define NEURALNETWORKCONFIGURATION_NETWORKCATEGORY_H

#include <QString>
#include "precompiled.h"

class NetworkCategory
{
    QX_REGISTER_FRIEND_CLASS(NetworkCategory)
public:
    NetworkCategory();
    ~NetworkCategory();

    void setId(const QString &id);
    QString getId();

    void setName(const QString &name);
    QString getName();

protected:
    QString id_{};
    QString name_{};
};

QX_REGISTER_PRIMARY_KEY(NetworkCategory, QString) //主键不是整形的时候使用
QX_REGISTER_HPP_APP(NetworkCategory, qx::trait::no_base_class_defined, 1)

#endif //NEURALNETWORKCONFIGURATION_NETWORKCATEGORY_H
