
#include "networkparams.h"

QX_REGISTER_CPP_APP(NetworkParams)

namespace qx
{
    template <> void register_class(QxClass<NetworkParams> & t)
    {
        t.setName("network_params");
        t.data(&NetworkParams::subclass_name_, "subclass_name");
        t.data(&NetworkParams::param_name_, "param_name");
        t.data(&NetworkParams::param_values_, "param_values");
        t.data(&NetworkParams::param_type_, "param_type");
    }
}

NetworkParams::NetworkParams()
{

}

NetworkParams::~NetworkParams()
{

}

void NetworkParams::setSubclassName(const QString &name)
{
    subclass_name_ = name;
}

QString NetworkParams::getSubclassName()
{
    return subclass_name_;
}

void NetworkParams::setParamName(const QString &name)
{
    param_name_ = name;
}

QString NetworkParams::getParamName()
{
    return param_name_;
}

void NetworkParams::setParamValues(const QString &value)
{
    param_values_ = value;
}

QString NetworkParams::getParamValues()
{
    return param_values_;
}

void NetworkParams::setParamType(const int &type)
{
    param_type_ = type;
}

int NetworkParams::getParamType()
{
    return param_type_;
}
