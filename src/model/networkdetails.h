
#ifndef NEURALNETWORKCONFIGURATION_NETWORKDETAILS_H
#define NEURALNETWORKCONFIGURATION_NETWORKDETAILS_H

#include <QString>
#include "precompiled.h"

class NetworkDetails
{
    QX_REGISTER_FRIEND_CLASS(NetworkCategory)
public:
    NetworkDetails();
    ~NetworkDetails();

    void setSubclassName(const QString &name);
    QString getSubclassName();

    void setTopPoints(const QString &points);
    QString getTopPoints();

    void setBottomPoints(const QString &points);
    QString getBottomPoints();

    void setLeftPoints(const QString &points);
    QString getLeftPoints();

    void setRightPoints(const QString &points);
    QString getRightPoints();

    void setPicPath(const QString &path);
    QString getPicPath();

    void setCanRepeat(const int &can);
    int  getCanRepeat();

    void setClassName(const QString &name);
    QString getClassName();

protected:
    QString subclass_name_{};
    QString top_points_{};
    QString bottom_points_{};
    QString left_points_{};
    QString right_points_{};
    QString pic_path_{};
    int     can_repeat_{};
    QString class_name_{};
};

QX_REGISTER_PRIMARY_KEY(NetworkDetails, QString) //主键不是整形的时候使用
QX_REGISTER_HPP_APP(NetworkDetails, qx::trait::no_base_class_defined, 1)

#endif //NEURALNETWORKCONFIGURATION_NETWORKDETAILS_H
