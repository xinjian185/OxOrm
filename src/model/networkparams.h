
#ifndef NEURALNETWORKCONFIGURATION_NETWORKPARAMS_H
#define NEURALNETWORKCONFIGURATION_NETWORKPARAMS_H

#include <QString>
#include "precompiled.h"

class NetworkParams
{
    QX_REGISTER_FRIEND_CLASS(NetworkParams)
public:
    NetworkParams();
    ~NetworkParams();

    void setSubclassName(const QString &name);
    QString getSubclassName();

    void setParamName(const QString &name);
    QString getParamName();

    void setParamValues(const QString &value);
    QString getParamValues();

    void setParamType(const int &type);
    int  getParamType();

protected:
    QString subclass_name_;
    QString param_name_;
    QString param_values_;
    int     param_type_;
};

// QX_REGISTER_PRIMARY_KEY(NetworkCategory, QString) //主键不是整形的时候使用
QX_REGISTER_HPP_APP(NetworkParams, qx::trait::no_base_class_defined, 1)

#endif //NEURALNETWORKCONFIGURATION_NETWORKPARAMS_H
