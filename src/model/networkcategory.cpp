
#include "networkcategory.h"

QX_REGISTER_CPP_APP(NetworkCategory)

namespace qx
{
    template <> void register_class(QxClass<NetworkCategory> & t)
    {
        // 映射表名称
        t.setName("network_category");
        // 映射主键
        t.id(&NetworkCategory::id_, "id");
        // 映射字段
        t.data(&NetworkCategory::name_, "name");
    }
}

NetworkCategory::NetworkCategory()
{

}

NetworkCategory::~NetworkCategory()
{

}

void NetworkCategory::setId(const QString &id)
{
    id_ = id;
}

QString NetworkCategory::getId()
{
    return id_;
}

void NetworkCategory::setName(const QString &name)
{
    name_ = name;
}

QString NetworkCategory::getName()
{
    return name_;
}


