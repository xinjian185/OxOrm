
#include "networkdetails.h"

QX_REGISTER_CPP_APP(NetworkDetails)

namespace qx
{
    template <> void register_class(QxClass<NetworkDetails> & t)
    {
        t.setName("network_details");
        t.id(&NetworkDetails::subclass_name_, "subclass_name");
        t.data(&NetworkDetails::top_points_, "top_points");
        t.data(&NetworkDetails::bottom_points_, "bottom_points");
        t.data(&NetworkDetails::left_points_, "left_points");
        t.data(&NetworkDetails::right_points_, "right_points");
        t.data(&NetworkDetails::pic_path_, "pic_path");
        t.data(&NetworkDetails::can_repeat_, "can_repeat");
        t.data(&NetworkDetails::class_name_, "class_name");
    }
}

NetworkDetails::NetworkDetails()
{

}

NetworkDetails::~NetworkDetails()
{

}

void NetworkDetails::setSubclassName(const QString &name)
{
    subclass_name_ = name;
}

QString NetworkDetails::getSubclassName()
{
    return subclass_name_;
}

void NetworkDetails::setTopPoints(const QString &points)
{
    top_points_ = points;
}

QString NetworkDetails::getTopPoints()
{
    return top_points_;
}

void NetworkDetails::setBottomPoints(const QString &points)
{
    bottom_points_ = points;
}

QString NetworkDetails::getBottomPoints()
{
    return bottom_points_;
}

void NetworkDetails::setLeftPoints(const QString &points)
{
    left_points_ = points;
}

QString NetworkDetails::getLeftPoints()
{
    return left_points_;
}

void NetworkDetails::setRightPoints(const QString &points)
{
    right_points_ = points;
}

QString NetworkDetails::getRightPoints()
{
    return right_points_;
}

void NetworkDetails::setCanRepeat(const int &can)
{
    can_repeat_ = can;
}

int NetworkDetails::getCanRepeat()
{
    return can_repeat_;
}

void NetworkDetails::setClassName(const QString &name)
{
    class_name_ = name;
}

QString NetworkDetails::getClassName()
{
    return class_name_;
}

void NetworkDetails::setPicPath(const QString &path)
{
    pic_path_ = path;
}

QString NetworkDetails::getPicPath()
{
    return pic_path_;
}
