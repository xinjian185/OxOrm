
#include "networksubclass.h"
QX_REGISTER_CPP_APP(NetworkSubclass)

namespace qx
{
    template <> void register_class(QxClass<NetworkSubclass> & t)
    {
        t.setName("network_subclass");
        t.id(&NetworkSubclass::name_, "name");
        t.data(&NetworkSubclass::category_id_, "category_id");
        t.data(&NetworkSubclass::picture_path_, "picture_path");
        t.data(&NetworkSubclass::usable_, "useable");
    }
}

NetworkSubclass::NetworkSubclass()
{

}

NetworkSubclass::~NetworkSubclass()
{

}

void NetworkSubclass::setCategoryId(const QString &name)
{
    category_id_ = name;
}

QString NetworkSubclass::getCategoryId()
{
    return category_id_;
}

void NetworkSubclass::setName(const QString &name)
{
    name_ = name;
}

QString NetworkSubclass::getName()
{
    return name_;
}

void NetworkSubclass::setPicturePath(const QString &path)
{
    picture_path_ = path;
}

QString NetworkSubclass::getPicturePath()
{
    return picture_path_;
}

void NetworkSubclass::setUsable(const int &usable)
{
    usable_  = usable;
}

int NetworkSubclass::getUsable()
{
    return usable_;
}
