
#ifndef NEURALNETWORKCONFIGURATION_NETWORKSUBCLASS_H
#define NEURALNETWORKCONFIGURATION_NETWORKSUBCLASS_H

#include <QString>
#include "precompiled.h"

class NetworkSubclass
{
    QX_REGISTER_FRIEND_CLASS(NetworkSubclass)
public:
    NetworkSubclass();
    ~NetworkSubclass();

    void setCategoryId(const QString &name);
    QString getCategoryId();

    void setName(const QString &name);
    QString getName();

    void setPicturePath(const QString &path);
    QString getPicturePath();

    void setUsable(const int &usable);
    int getUsable();

protected:
    QString category_id_;
    QString name_;
    QString picture_path_;
    int     usable_;
};

QX_REGISTER_PRIMARY_KEY(NetworkSubclass, QString) //主键不是整形的时候使用
QX_REGISTER_HPP_APP(NetworkSubclass, qx::trait::no_base_class_defined, 1)

#endif //NEURALNETWORKCONFIGURATION_NETWORKSUBCLASS_H
