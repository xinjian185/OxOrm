
#include "paramswidget.h"
#include <QVBoxLayout>
#include <QHeaderView>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QMessageBox>
#include "networkparams.h"

ParamsWidget::ParamsWidget(QWidget *parent)
{
    initUi();
}

ParamsWidget::~ParamsWidget()
{

}

void ParamsWidget::initUi()
{
    QVBoxLayout *layout2 = new QVBoxLayout(this);
    QHBoxLayout *layout1 = new QHBoxLayout(this);
    QHBoxLayout *layout3 = new QHBoxLayout(this);
    setLayout(layout2);
    subclass_box_ = new QComboBox(this);
    connect(subclass_box_, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentTextChanged),this,&ParamsWidget::getAllParamsBySubclass);
    table_params_ = new QTableWidget(this);
    param_name_line_ = new QLineEdit(this);
    param_values_line_ = new QLineEdit(this);
    param_type_box_ = new QComboBox(this);
    param_type_box_->addItem("输入框",0);
    param_type_box_->addItem("列表",1);
    QLabel *label1 = new QLabel("参数名称",this);
    QLabel *label2 = new QLabel("参数类型",this);
    QLabel *label3 = new QLabel("默认值",this);
    QPushButton *addBtn = new QPushButton("添加",this);
    QPushButton *deleteBtn = new QPushButton("删除",this);
    connect(addBtn,&QPushButton::clicked,this,&ParamsWidget::addParam);
    connect(deleteBtn,&QPushButton::clicked,this,&ParamsWidget::deleteParam);
    layout3->addWidget(label1);
    layout3->addWidget(param_name_line_);
    layout3->addWidget(label2);
    layout3->addWidget(param_type_box_);
    layout3->addWidget(label3);
    layout3->addWidget(param_values_line_);
    layout3->addWidget(addBtn);
    layout3->addWidget(deleteBtn);
    layout1->addWidget(subclass_box_);
    layout2->addItem(layout1);
    layout2->addItem(layout3);
    layout2->addWidget(table_params_);
    table_params_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table_params_->setSelectionBehavior(QTableWidget::SelectRows);
    table_params_->setSelectionMode(QTableWidget::SingleSelection);
    table_params_->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table_params_->setColumnCount(3);
    table_params_->setHorizontalHeaderLabels({"参数","类型","默认值"});
}

void ParamsWidget::subclassChanged(const QStringList &list)
{
    subclass_box_->clear();
    subclass_box_->addItems(list);
}

void ParamsWidget::getAllParamsBySubclass(const QString &text)
{
    table_params_->clearContents();
    table_params_->setRowCount(0);
    QList<NetworkParams> params;
    QString sql = QString("select * from network_params where subclass_name = '%1'").arg(text);
    qx::QxSqlQuery query(sql);
    qx::dao::execute_query(query,params);
    for(NetworkParams & param : params)
    {
        int row = table_params_->rowCount();
        table_params_->insertRow(row);
        QString name = param.getParamName();
        int     type = param.getParamType();
        QString values = param.getParamValues();
        table_params_->setItem(row,0,new QTableWidgetItem(name));
        table_params_->setItem(row,1,new QTableWidgetItem(QString::number(type)));
        table_params_->setItem(row,2,new QTableWidgetItem(values));
    }
}

void ParamsWidget::addParam()
{
    if(param_name_line_->text().isEmpty())
    {
        QMessageBox::critical(this,"error","参数名称不能为空");
        return;
    }
    NetworkParams param;
    param.setSubclassName(subclass_box_->currentText());
    param.setParamName(param_name_line_->text());
    param.setParamType(param_type_box_->currentData().toInt());
    param.setParamValues(param_values_line_->text());
    qx::dao::insert(param);
    getAllParamsBySubclass(subclass_box_->currentText());
}

void ParamsWidget::deleteParam()
{
    QList<QTableWidgetItem*> items = table_params_->selectedItems();
    if(items.isEmpty())
    {
        QMessageBox::critical(this,"error","请选择需要删除的项目");
        return;
    }
    QString name = items[0]->text();
    QString subclassName = subclass_box_->currentText();
    NetworkParams param;
    param.setSubclassName(subclass_box_->currentText());
    param.setParamName(name);
    QString sql = QString("delete from network_params where subclass_name = '%1' and param_name = '%2'").arg(subclassName).arg(name);
    qx::QxSqlQuery query(sql);
    QSqlError daoError = qx::dao::execute_query(query,param);
    if(daoError.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",daoError.text());
        return;
    }
    getAllParamsBySubclass(subclassName);
}
