
#include "mainwindow.h"
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QHeaderView>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    initUi();
}

MainWindow::~MainWindow()
{

}

void MainWindow::initUi()
{
    setMinimumSize(1366,768);
    tab_widget_ = new QTabWidget(this);
    setCentralWidget(tab_widget_);
    category_widget_ = new CategoryWidget(this);
    subclass_widget_ = new SubclassWidget(this);
    params_widget_ = new ParamsWidget(this);
    connect(category_widget_,&CategoryWidget::sig_categoryChange,subclass_widget_,&SubclassWidget::categoryChanged);
    connect(subclass_widget_,&SubclassWidget::sig_subclassChange,params_widget_,&ParamsWidget::subclassChanged);
    category_widget_->getCategoryList();
    subclass_widget_->getSubclass();
    // 大类设置界面
    tab_widget_->addTab(category_widget_,"大类维护");
    // 子类设置界面
    tab_widget_->addTab(subclass_widget_,"子类维护");
    // 参数维护界面
    tab_widget_->addTab(params_widget_,"参数维护");

}

