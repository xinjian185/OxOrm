
#ifndef NEURALNETWORKCONFIGURATION_CATEGORYWIDGET_H
#define NEURALNETWORKCONFIGURATION_CATEGORYWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QTableWidget>
#include "networkcategory.h"

class CategoryWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CategoryWidget(QWidget *parent = nullptr);
    ~CategoryWidget() override;

    void getCategoryList();

signals:
    void sig_categoryChange(const QStringList &list);

protected:
    void initUi();
    void getNetworkCategories();

protected slots:
    void addCategory();
    void deleteCategory();

private:
    QLineEdit    *category_line_{nullptr};
    QTableWidget *table_category_{nullptr};

    QList<NetworkCategory> network_categories_;
};


#endif //NEURALNETWORKCONFIGURATION_CATEGORYWIDGET_H
