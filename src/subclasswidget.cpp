
#include "subclasswidget.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QHeaderView>
#include <QMessageBox>
#include <QFileDialog>
#include "networksubclass.h"
#include "networkdetails.h"

SubclassWidget::SubclassWidget(QWidget *parent)
{
    initUi();
}

SubclassWidget::~SubclassWidget()
{

}

void SubclassWidget::initUi()
{
    table_subclass_ = new QTableWidget(this);
    category_box_ = new QComboBox(this);
    can_repeat_box_ = new QComboBox(this);
    top_point_line_ = new QLineEdit(this);
    bottom_point_line_ = new QLineEdit(this);
    left_point_line_ = new QLineEdit(this);
    right_point_line_ = new QLineEdit(this);
    class_name_line_ = new QLineEdit(this);
    QVBoxLayout *layout2 = new QVBoxLayout(this);
    QHBoxLayout *hLayout2 = new QHBoxLayout(this);
    QHBoxLayout *hLayout4 = new QHBoxLayout(this);
    QHBoxLayout *hLayout3 = new QHBoxLayout(this);
    category_box_->setParent(this);
    subclass_useful_box_ = new QComboBox(this);
    subclass_useful_box_->addItem("可用",1);
    subclass_useful_box_->addItem("不可用",0);
    can_repeat_box_->addItem("不可",0);
    can_repeat_box_->addItem("可",1);
    QLabel *labelSubclass1 = new QLabel("大类名称",this);
    QLabel *labelSubclass2 = new QLabel("子类名称",this);
    QLabel *labelSubclass3 = new QLabel("是否可用",this);
    subclass_line_ = new QLineEdit(this);
    subclass_pic_line_ = new QLineEdit(this);
    subclass_pic_line_->setReadOnly(true);
    subclass_pic_line_->setPlaceholderText("图片路径，请选择图片");
    setLayout(layout2);
    QPushButton *addSubclassBtn = new  QPushButton("添加",this);
    QPushButton *disableSubclassBtn = new  QPushButton("禁用",this);
    QPushButton *enableSubclassBtn = new  QPushButton("启用",this);
    QPushButton *picSubclassBtn = new  QPushButton("选择图片",this);
    QLabel *topLabel = new  QLabel("上端点",this);
    QLabel *bottomLabel = new  QLabel("下端点",this);
    QLabel *leftLabel = new  QLabel("左端点",this);
    QLabel *rightLabel = new  QLabel("右端点",this);
    QLabel *repeatLabel = new  QLabel("重复接入",this);
    QLabel *classLabel = new  QLabel("映射名称",this);
    connect(addSubclassBtn,&QPushButton::clicked,this,&SubclassWidget::addSubclass);
    connect(disableSubclassBtn,&QPushButton::clicked,this,&SubclassWidget::disableSubclass);
    connect(picSubclassBtn,&QPushButton::clicked,this,&SubclassWidget::picSubclass);
    connect(enableSubclassBtn,&QPushButton::clicked,this,&SubclassWidget::enableSubclass);
    hLayout2->addWidget(labelSubclass1);
    hLayout2->addWidget(category_box_);
    hLayout2->addWidget(labelSubclass2);
    hLayout2->addWidget(subclass_line_);
    hLayout2->addWidget(subclass_pic_line_);
    hLayout2->addWidget(picSubclassBtn);
    hLayout2->addWidget(labelSubclass3);
    hLayout2->addWidget(subclass_useful_box_);
    hLayout3->addWidget(addSubclassBtn);
    hLayout3->addWidget(disableSubclassBtn);
    hLayout3->addWidget(enableSubclassBtn);

    hLayout4->addWidget(topLabel);
    hLayout4->addWidget(top_point_line_);
    hLayout4->addWidget(bottomLabel);
    hLayout4->addWidget(bottom_point_line_);
    hLayout4->addWidget(leftLabel);
    hLayout4->addWidget(left_point_line_);
    hLayout4->addWidget(rightLabel);
    hLayout4->addWidget(right_point_line_);
    hLayout4->addWidget(repeatLabel);
    hLayout4->addWidget(can_repeat_box_);
    hLayout4->addWidget(classLabel);
    hLayout4->addWidget(class_name_line_);

    layout2->addItem(hLayout2);
    layout2->addItem(hLayout4);
    layout2->addItem(hLayout3);
    layout2->addWidget(table_subclass_);
    table_subclass_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table_subclass_->setSelectionBehavior(QTableWidget::SelectRows);
    table_subclass_->setSelectionMode(QTableWidget::SingleSelection);
    table_subclass_->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    table_subclass_->setColumnCount(10);
    table_subclass_->setHorizontalHeaderLabels({"大类名称","子类名称","图片路径","是否可用","上端点","下端点","左端点","右端点","是否可重复","对照名称"});
    getNetworkSubclass();
}

void SubclassWidget::addSubclass()
{
    if(subclass_line_->text().isEmpty())
    {
        QMessageBox::critical(this,"error","子类名称不可为空");
        return;
    }
    if(subclass_pic_line_->text().isEmpty())
    {
        auto ret = QMessageBox::question(this,"question","您未选择加载的图片，是否放弃选择？",QMessageBox::Yes|QMessageBox::No);
        if(ret != QMessageBox::Yes)
        {
            return;
        }
    }

    NetworkSubclass subclass;
    QString categoryName = category_box_->currentText();
    QString name = subclass_line_->text();
    QString picturePath =subclass_pic_line_->text();
    int usable =subclass_useful_box_->currentData().toInt();
    subclass.setName(name);
    subclass.setCategoryId(categoryName);
    subclass.setPicturePath(picturePath);
    subclass.setUsable(usable);

    QString bottomPoint = bottom_point_line_->text();
    QString topPoint = top_point_line_->text();
    QString leftPoint = left_point_line_->text();
    QString rightPoint = right_point_line_->text();
    QString className = class_name_line_->text();
    int repeat =can_repeat_box_->currentData().toInt();

    NetworkDetails details;
    details.setPicPath(picturePath);
    details.setBottomPoints(bottomPoint);
    details.setTopPoints(topPoint);
    details.setSubclassName(name);
    details.setLeftPoints(leftPoint);
    details.setRightPoints(rightPoint);
    details.setCanRepeat(repeat);
    details.setClassName(className);


    QSqlError error = qx::dao::insert(subclass);
    if(error.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",error.text());
    }
    error = qx::dao::insert(details);
    if(error.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",error.text());
    }
    getNetworkSubclass();
}

void SubclassWidget::disableSubclass()
{
    QList<QTableWidgetItem*> items = table_subclass_->selectedItems();
    if(items.isEmpty())
    {
        QMessageBox::critical(this,"error","请选择需要禁用的项目");
        return;
    }
    if(items.count() > 4)
    {
        QMessageBox::critical(this,"error","不可以一次修改多条数据");
        return;
    }
    QString usable = items[3]->text();
    if(usable == "0")
    {
        QMessageBox::critical(this,"error","项目已经被禁用");
        return;
    }
    NetworkSubclass subclass;
    QString categoryName = items[0]->text();
    QString name = items[1]->text();
    QString picturePath = items[2]->text();
    subclass.setName(name);
    subclass.setCategoryId(categoryName);
    subclass.setPicturePath(picturePath);
    subclass.setUsable(0);
    QSqlError error = qx::dao::update(subclass);
    if(error.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",error.text());
    }
    getNetworkSubclass();
}

void SubclassWidget::picSubclass()
{
    QString path = QFileDialog::getOpenFileName(this,"open png","","*.PNG");
    if(path.isEmpty())
    {
        return;
    }
    subclass_pic_line_->setText(path);
}

void SubclassWidget::categoryChanged(const QStringList &list)
{
    category_box_->clear();
    category_box_->addItems(list);
}

void SubclassWidget::getNetworkSubclass()
{
    table_subclass_->clearContents();
    table_subclass_->setRowCount(0);
    subclasses_.clear();
    qx::dao::fetch_all(subclasses_);
    for(NetworkSubclass & subclass : subclasses_)
    {
        int row = table_subclass_->rowCount();
        table_subclass_->insertRow(row);
        QString categoryName = subclass.getCategoryId();
        QString name = subclass.getName();
        QString picturePath = subclass.getPicturePath();
        int usable = subclass.getUsable();
        table_subclass_->setItem(row,0,new QTableWidgetItem(categoryName));
        table_subclass_->setItem(row,1,new QTableWidgetItem(name));
        table_subclass_->setItem(row,2,new QTableWidgetItem(picturePath));
        table_subclass_->setItem(row,3,new QTableWidgetItem(QString::number(usable)));
        NetworkDetails details;
        details.setSubclassName(name);
        qx::dao::fetch_by_id(details);
        table_subclass_->setItem(row,4,new QTableWidgetItem(details.getTopPoints()));
        table_subclass_->setItem(row,5,new QTableWidgetItem(details.getBottomPoints()));
        table_subclass_->setItem(row,6,new QTableWidgetItem(details.getLeftPoints()));
        table_subclass_->setItem(row,7,new QTableWidgetItem(details.getRightPoints()));
        table_subclass_->setItem(row,8,new QTableWidgetItem(QString::number(details.getCanRepeat())));
        table_subclass_->setItem(row,9,new QTableWidgetItem(details.getClassName()));
    }
    getSubclass();
}

void SubclassWidget::enableSubclass()
{
    QList<QTableWidgetItem*> items = table_subclass_->selectedItems();
    if(items.isEmpty())
    {
        QMessageBox::critical(this,"error","请选择需要启用的项目");
        return;
    }
    if(items.count() > 4)
    {
        QMessageBox::critical(this,"error","不可以一次修改多条数据");
        return;
    }
    QString usable = items[3]->text();
    if(usable == "1")
    {
        QMessageBox::critical(this,"error","项目未被禁用");
        return;
    }
    NetworkSubclass subclass;
    QString categoryName = items[0]->text();
    QString name = items[1]->text();
    QString picturePath = items[2]->text();
    subclass.setName(name);
    subclass.setCategoryId(categoryName);
    subclass.setPicturePath(picturePath);
    subclass.setUsable(1);
    QSqlError error = qx::dao::update(subclass);
    if(error.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",error.text());
    }
    getNetworkSubclass();
}

void SubclassWidget::getSubclass()
{
    QStringList list;

    for(NetworkSubclass & subclass : subclasses_)
    {
        if(subclass.getUsable())
        {
            list.append(subclass.getName());
        }
    }
    emit sig_subclassChange(list);
}
