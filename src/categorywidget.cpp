
#include "categorywidget.h"
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QHeaderView>
#include <QMessageBox>

CategoryWidget::CategoryWidget(QWidget *parent)
{
    initUi();
}

CategoryWidget::~CategoryWidget()
{

}

void CategoryWidget::initUi()
{
    table_category_ = new QTableWidget(this);
    QVBoxLayout *layout = new QVBoxLayout(this);
    QHBoxLayout *hLayout1 = new QHBoxLayout(this);
    QLabel *labelCategory = new QLabel("大类名称",this);
    setLayout(layout);
    category_line_ = new QLineEdit(this);
    QPushButton *addCategoryBtn = new  QPushButton("添加",this);
    QPushButton *deleteCategoryBtn = new  QPushButton("删除",this);
    connect(addCategoryBtn,&QPushButton::clicked,this,&CategoryWidget::addCategory);
    connect(deleteCategoryBtn,&QPushButton::clicked,this,&CategoryWidget::deleteCategory);
    hLayout1->addWidget(labelCategory);
    hLayout1->addWidget(category_line_);
    hLayout1->addWidget(addCategoryBtn);
    hLayout1->addWidget(deleteCategoryBtn);
    layout->addItem(hLayout1);
    layout->addWidget(table_category_);
    table_category_->setEditTriggers(QAbstractItemView::NoEditTriggers);
    table_category_->setSelectionBehavior(QTableWidget::SelectRows);
    table_category_->setSelectionMode(QTableWidget::SingleSelection);
    table_category_->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    table_category_->setColumnCount(1);
    table_category_->setHorizontalHeaderLabels({"大类名称"});
    getNetworkCategories();
}

void CategoryWidget::getNetworkCategories()
{
    table_category_->clearContents();
    table_category_->setRowCount(0);
    network_categories_.clear();
    qx::dao::fetch_all(network_categories_);
    for(NetworkCategory & network_category : network_categories_)
    {
        int row = table_category_->rowCount();
        table_category_->insertRow(row);
        QString name = network_category.getId();
        table_category_->setItem(row,0,new QTableWidgetItem(name));

    }
    getCategoryList();
}

void CategoryWidget::addCategory()
{
    QString name = category_line_->text();
    if(name.isEmpty())
    {
        QMessageBox::critical(this,"error","大类名称不能为空");
        return;
    }
    NetworkCategory category;
    category.setName(name);
    category.setId(name);
    QSqlError daoError = qx::dao::insert(category);
    if(daoError.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",daoError.text());
        return;
    }
    getNetworkCategories();
}

void CategoryWidget::deleteCategory()
{
    QList<QTableWidgetItem*> items = table_category_->selectedItems();
    if(items.isEmpty())
    {
        QMessageBox::critical(this,"error","请选择需要删除的项目");
        return;
    }
    if(items.count() > 1)
    {
        QMessageBox::critical(this,"error","不可以一次删除多条数据");
        return;
    }
    QString name = items[0]->text();
    NetworkCategory category;
    category.setName(name);
    category.setId(name);
    QSqlError daoError = qx::dao::delete_by_id(category);
    if(daoError.type() != QSqlError::NoError)
    {
        QMessageBox::critical(this,"error",daoError.text());
        return;
    }
    getNetworkCategories();
}

void CategoryWidget::getCategoryList()
{
    QStringList list;
    for(NetworkCategory & network_category : network_categories_)
    {
        QString name = network_category.getId();
        list.append(name);
    }
    emit sig_categoryChange(list);
}
